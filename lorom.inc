; Sets up processor basics and ROM headers/vectors

.p816   ; 65816 processor
.i16    ; X/Y are 16 bits
.a8     ; A is 8 bits

.segment "HEADER"     ; +$7FE0 in file
    .byte "SHOOTY FX            "

.segment "ROMINFO"  ; +$7FD5 in file
    .byte $20       ; LoROM
    .byte $14       ; LoROM + DSP + RAM
    .byte $07       ; 128K ROM
    .byte $05       ; 32K SRAM
    .byte $01       ; USA region
    .byte $33       ; maker byte $69 (nice)
    .byte $00       ; Mask revision 0
    .word $AAAA,$5555 ; dummy checksum and complement

.segment "EXTENDED" ; +$7FB0 in file
    .byte "01"         ; 2 ASCII byte maker code
    .byte "SHFX"       ; 4 ASCII byte game code
    .byte 0,0,0,0,0,0   ; 6 reserved bytes
    .byte 0             ; expansion flash memory
    .byte $06           ; expansion RAM (1 << N KBytes)
    .byte $00           ; Special Version
    .byte $00           ; Chipset sub-type

.segment "VECTORS"
    .word 0, 0, 0, 0, 0, VBLANK, reset, 0
    .word 0, 0, 0, 0, 0, VBLANK, reset, 0

.code

.macro init_cpu
    clc
    xce
    rep #$10        ; X/Y 16-bit
    sep #$20        ; A 8-bit
.endmacro
