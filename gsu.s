.import MDMAEN, DMACH1DEST, DMACH1SETTINGS, DMACH1BYTES, DMACH1BANK, DMACH1OFFSET, VMADDR, VMAINC
.import SFX_SFR
.import LoadVRAM

.export GSU_CopyGSUBufferToBG2_ONE
.export GSU_CopyGSUBufferToBG2_TWO
.export GSU_CopyGSUBufferToBG2
.export GSU_CopyGSUBufferToBG3

.export GSU_LoadProgramIntoSRAM
.export GSU_WaitForProgram

	.include "65816.inc"

	.a8
	.i16

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
GSU_CopyGSUBufferToBG2_ONE:

	lda #$80
	sta VMAINC 	; write words, normal increment

	; Transfer from SRAM $70:4000 to VRAM $8000
	ldx	#$4000	; Destination address in words
	stx	VMADDR	; save
	lda #$70	; source bank
	ldx	#$4000	; source offset
	ldy	#$2400	; size to transfer
	; a GSU screen is 256x192x4. that comes out to $6000 bytes.
	; a full frame is $6000
	; 2/3 of a frame is 192x192. that is $4800 bytes

	stx DMACH1OFFSET
	sta DMACH1BANK
	sty DMACH1BYTES

	lda #$01
	sta DMACH1SETTINGS 	; DMA mode: word, normal increment
	lda #$18
	sta DMACH1DEST 		; write to VRAM data input
	lda #%00000010
	sta MDMAEN	; begin DMA transfer on Channel 1

	rts

GSU_CopyGSUBufferToBG2_TWO:

	lda #$80
	sta VMAINC 	; write words, normal increment

	; Transfer from SRAM $70:4000 to VRAM $8000
	ldx	#$5200	; Destination address in words
	stx	VMADDR	; save
	lda #$70	; source bank
	ldx	#$6400	; source offset
	ldy	#$2400	; size to transfer
	; a GSU screen is 256x192x4. that comes out to $6000 bytes.
	; a full frame is $6000
	; 2/3 of a frame is 192x192. that is $4800 bytes

	stx DMACH1OFFSET
	sta DMACH1BANK
	sty DMACH1BYTES

	lda #$01
	sta DMACH1SETTINGS 	; DMA mode: word, normal increment
	lda #$18
	sta DMACH1DEST 		; write to VRAM data input
	lda #%00000010
	sta MDMAEN	; begin DMA transfer on Channel 1

	rts

GSU_CopyGSUBufferToBG2:

	lda #$80
	sta VMAINC 	; write words, normal increment

	; Transfer from SRAM $70:4000 to VRAM $8000
	ldx	#$4000	; Destination address in words
	stx	VMADDR	; save
	lda #$70	; source bank
	ldx	#$4000	; source offset
	ldy	#$4800	; size to transfer
	; a GSU screen is 256x192x4. that comes out to $6000 bytes.
	; a full frame is $6000
	; 2/3 of a frame is 192x192. that is $4800 bytes

	stx DMACH1OFFSET
	sta DMACH1BANK
	sty DMACH1BYTES

	lda #$01
	sta DMACH1SETTINGS 	; DMA mode: word, normal increment
	lda #$18
	sta DMACH1DEST 		; write to VRAM data input
	lda #%00000010
	sta MDMAEN	; begin DMA transfer on Channel 1

	rts

;;;;;;;

GSU_CopyGSUBufferToBG3:

	lda #$80
	sta VMAINC 	; write words, normal increment

	; Transfer from SRAM $70:4000 to VRAM $8000
	ldx	#$6000	; Destination address in words
	stx	VMADDR	; save
	lda #$70	; source bank
	ldx	#$4000	; source offset
	ldy	#$2400	; size to transfer
	; a GSU screen is 256x192x4. that comes out to $6000 bytes.
	; a full frame is $6000
	; 2/3 of a frame is 192x192. that is $4800 bytes

	stx DMACH1OFFSET
	sta DMACH1BANK
	sty DMACH1BYTES

	lda #$01
	sta DMACH1SETTINGS 	; DMA mode: word, normal increment
	lda #$18
	sta DMACH1DEST 		; write to VRAM data input
	lda #%00000010
	sta MDMAEN	; begin DMA transfer on Channel 1

	rts

;;;;;;;

GSU_WaitForProgram:
	; Wait for the G flag to be cleared.
	; Spin until this is the case.

	lda	#$0020
	bit	SFX_SFR
	bne	GSU_WaitForProgram

	rts

;;;;;;;

GSU_LoadProgramIntoSRAM:
	GoACC16
	lda #$1000	; TODO: get the program length
	tax			; move it to X
	GoACC8

	phb

	lda	#$70
	pha
	plb

@sfx_copy:
	dex

	.import __BANK2_LOAD__

	lda f:__BANK2_LOAD__,X	; force far addressing since we change the data page
	sta a:$0000,x
	
	cpx	#$0000
	bne	@sfx_copy

	plb

	rts