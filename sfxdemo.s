; SuperFX program.
	.segment "BANK2"

	.include "superfx/casfx.inc"

	.export SuperFX_DoSomething
	.export SuperFX_DoSomething_End
	.export SuperFX_DoSomething_Length

	.macro gsu_PUSH reg
	; decrement stack pointer, push reg onto stack
	dec		r10
	dec		r10
	from	reg
	stw		(r10)
	.endmacro

	.macro gsu_POP reg
	; pop reg from stack, increment stack pointer
	to	 	reg
	ldw		(r10)
	inc		r10
	inc		r10
	.endmacro

	; JSR
	.macro gsu_JSR JDestination
	gsu_PUSH r11				; Push existing link destination onto stack.
	link	#4					; r11 <- instruction after the branch delay slot
	iwt		pc, JDestination 	; PC <- jump destination
	nop							; Branch delay slot
	gsu_POP r11					; Unlink the previous stack frame.
	.endmacro

	; RTS
	.macro gsu_RTS
	jmp		r11					; Jump to link destination.
	nop		; branch delay slot
	.endmacro

SuperFX_DoSomething:
	iwt	r10, #$FFFE	; set up the stack pointer

	ibt	r0, #0
	cmode
	
	;gsu_JSR	#ClearFramebuffer

	ibt	r0, #2
	from r0
	color

;	iwt	r3, #48
;	iwt	r4, #48
;	iwt r5, #148
;	iwt r6, #48
;	gsu_JSR	#DoBresenham

	iwt		r3, #degrees
	to		r2
	ldw		(r3)

	; if r2 is above 359, reset to 0
@checkRange:
	iwt		r0, #360
	with	r2
	cmp		r0
	blt		@doRotation
	nop

	iwt		r2, #$FFFF

@doRotation:
;	inc		r2
	from	r2
	stw		(r3)

	; Rotate (144, 144) by 45 degrees around (96, 96) and plot.
	iwt		r0, #((144-96) << 8)	; fixed point 8.8 value
	iwt		r1, #((144-96) << 8)	; fixed point 8.8 value
	iwt		r2, #degrees
	to		r2
	ldw		(r2)
	gsu_JSR #RotatePoint

	; Translate to be based on (96,96) as the origin.
	iwt		r3, #96
	to		r1
	from	r3
	add		r1
	to		r2
	from	r3
	add		r2

	iwt		r4, #RotatedX0
	from	r1
	stw		(r4)
	iwt		r4, #RotatedY0
	from	r2
	stw		(r4)

	plot	; and plot

	; Rotate (48, 144) by 45 degrees around (96, 96) and plot.
	iwt		r0, #(-48 << 8)		; fixed point 8.8 value
	iwt		r1, #(48 << 8)	; fixed point 8.8 value
	iwt		r2, #degrees
	to		r2
	ldw		(r2)
	gsu_JSR #RotatePoint

	; Translate to be based on (96,96) as the origin.
	iwt		r3, #96
	to		r1
	from	r3
	add		r1
	to		r2
	from	r3
	add		r2

	iwt		r4, #RotatedX1
	from	r1
	stw		(r4)
	iwt		r4, #RotatedY1
	from	r2
	stw		(r4)

	plot	; and plot

	;; Plot the rotated line...
	iwt		r0, #RotatedX0
	to		r3
	ldw		(r0)
	iwt		r0, #RotatedY0
	to		r4
	ldw		(r0)
	iwt		r0, #RotatedX1
	to		r5
	ldw		(r0)
	iwt		r0, #RotatedY1
	to		r6
	ldw		(r0)

	gsu_JSR	#DoBresenham

	; resume execution here when returning
	rpix

	stop

;;;

DrawRect:
	; x,y,h,w: r3,r4,r5,r6

	move r1,r3
	move r2,r4

	; draw the top line
	move r12,r6						; loop count
	iwt	 r13,#@horizontalLoopTop	; loop branch destination

@horizontalLoopTop:
	plot
	loop
	nop

	; draw the bottom line
	move r12,r6						; loop count
	iwt	 r13,#@horizontalLoopBottom	; loop branch destination

	move 	r1,r3
	move 	r2,r4
	to		r2
	from	r5
	add		r2						; MIPS: add r2,r5,r2

@horizontalLoopBottom:
	plot
	loop
	nop

	; draw the left line
	move	r1,r3
	move	r2,r4

	move 	r12,r5						; loop count
	iwt	 	r13,#@verticalLoopLeft 	; loop branch destination

@verticalLoopLeft:
	plot
	dec		r1
	inc		r2
	loop
	nop

	; draw the right line
	move 	r1,r3
	move 	r2,r4
	to		r1
	from	r6
	add		r1							; MIPS: add r1,r6,r1

	move 	r12,r5						; loop count
	iwt	 	r13,#@verticalLoopRight 	; loop branch destination
	inc		r12

@verticalLoopRight:
	plot
	dec		r1
	inc		r2
	loop
	nop

@done:
	gsu_RTS

;;;;;;;;;;;;;;
DoBresenham:
	; draw a line
	; (x0,y0) = r3,r4
	; (x1,y1) = r5,r6

@saveCoordinates:
	iwt		r0,#BresenhamX0
	from	r3
	stw		(r0)
	iwt		r0,#BresenhamX1
	from	r5
	stw		(r0)
	iwt		r0,#BresenhamY0
	from	r4
	stw		(r0)
	iwt		r0,#BresenhamY1
	from	r6
	stw		(r0)

	; Is the slope LOW or HIGH?
	; If y1-y0 < x1-x0, it is low. otherwise, it is high
@deltaY:
	to 		r7
	from 	r6
	sub		r4	; r7 = (y1-y0)
	; flip sign if negative
	bpl		@deltaX

	to		r7
	from	r7
	not		

@deltaX:
	to		r8
	from	r5
	sub		r3	; r8 = (x1-x0)
	; flip sign if negative
	bpl		@calc
	nop
	nop
	nop
	nop

	from	r8
	not		r8

@calc:
	; Optimization case: Vertical line (deltaX is 0)
	iwt		r0, #0
	from	r8
	cmp		r0
	bne		@selectRoutine

	gsu_JSR	#PlotLineVertical
	gsu_RTS	; done

@selectRoutine:
	from	r7
	cmp		r8	; (r7 - r8), so result is negative if (x1-x0) is greater

	bpl		@slopeIsHigh
	nop
	nop

@slopeIsLow:
	gsu_JSR	#PlotLineLow
	bra	@return
	nop

@slopeIsHigh:
	gsu_JSR	#PlotLineHigh
	bra	@return
	nop

@return:
	gsu_RTS

;;;;;;;;;;;;;;;;;;;;;;;
; 2D rotation
RotatePoint:
; Rotate a 2D point r2 degrees around (r0, r1).

	; Input:
	; r0: Point X
	; r1: Point Y
	; r2: Rotation degrees (0-359)
	;
	; Output:
	; r1: Point X'
	; r2: Point Y'
	;
	; Clobbers r0-r10 and r12.

	move	r5, r0	; r5 = X
	move	r12, r1	; r12 = Y

@sinX0:
	; Calculate sin(degrees) * X0
	iwt		r9,#SinTable

	move	r8,r2
	to		r0
	from	r0
	add		#0		; clear the carry bit
	rol		r8		; r8 is now the byte offset into SinTable

	to		r9
	from	r8
	add		r9		; r9 = pointer to SinTable[degrees]

	to		r6
	ldw		(r9)	; r6 <- SinTable[degrees]

	from	r5
	to		r0
	fmult	; perform 16x16 signed multiply, discard lower word of result

	iwt		r1,#TempSinX
	from	r0
	stw		(r1)	; save the high word into TempSinX

@cosX0:
	; Calculate cos(degrees) * X0
	iwt		r9, #CosTable

	move	r8,r2
	to		r0
	from	r0
	add		#0	; clear the carry bit
	rol		r8	; r8 is now the byte offset into CosTable

	to		r9
	from	r8
	add		r9		; r9 = pointer to SinTable[degrees]

	to		r6
	ldw		(r9)	; r6 <- SinTable[degrees]

	from	r5
	to		r0
	fmult	; perform 16x16 signed multiply, discard lower word of result

	iwt		r1,#TempCosX
	from	r0
	stw		(r1)	; save the high word into TempCosX

@sinY0:
	; Calculate sin(degrees) * Y0
	iwt		r9, #SinTable

	move	r8,r2
	to		r0
	from	r0
	add		#0	; clear the carry bit
	rol		r8	; r8 is now the byte offset into SinTable

	to		r9
	from	r8
	add		r9		; r9 = pointer to SinTable[degrees]

	to		r6
	ldw		(r9)	; r6 <- SinTable[degrees]

	from	r12
	to		r0
	fmult	; perform 16x16 signed multiply, discard lower word of result

	iwt		r1,#TempSinY
	from	r0
	stw		(r1)	; save the high word into TempSinY

@cosY0:
	; Calculate cos(degrees) * Y0
	iwt		r9, #CosTable

	move	r8,r2
	to		r0
	from	r0
	add		#0	; clear the carry bit
	rol		r8	; r8 is now the byte offset into CosTable

	to		r9
	from	r8
	add		r9		; r9 = pointer to SinTable[degrees]

	to		r6
	ldw		(r9)	; r6 <- SinTable[degrees]

	from	r12
	to		r0
	fmult	; perform 16x16 signed multiply, discard lower word of result

	iwt		r1,#TempCosY
	from	r0
	stw		(r1)	; save the high word into TempCosY

; Calculate (X',Y') for the start pixel.
	; x' = x*(cos(deg)) - y*(sin(deg))
	; y' = y*(cos(deg)) + x*(sin(deg))
	
	iwt		r1,#TempCosX
	to		r0
	ldw		(r1)

	iwt		r2,#TempSinY
	to		r1
	ldw		(r2)

	to		r1
	from	r0
	sub		r1	; r1 = r0-r1

	;;;

	iwt		r3,#TempCosY
	to		r2
	ldw		(r3)

	iwt		r4,#TempSinX
	to		r3
	ldw		(r4)

	to		r2
	from	r3
	add		r2	; r2 = r3+r2

	; X' and Y' are returned in r1/r2.

	gsu_RTS

;;;;;;;;
; Line plot routines
PlotLineVertical:
	; plot a vertical line of length deltaY
	move 	r12,r7					; loop count
	inc		r12
	iwt	 	r13,#@verticalLoop	 	; loop branch destination

	; switch y1 and y0 if y0 is larger
	to		r0
	from	r6
	sub		r4
	bmi		@useY1
	nop

@useY0:
	; draw the left line
	move	r1,r3
	move	r2,r4

	bra		@verticalLoop
	nop

@useY1:
	move	r1,r3
	move	r2,r6

@verticalLoop:
	plot
	dec		r1
	inc		r2
	loop
	nop

	gsu_RTS

;;;;;;;;;;;;

PlotLineLow:
	gsu_PUSH	r11

@checkSwapX:
	; ensure x1 > x0
	to		r8
	from	r5
	sub		r3	; r8 = (x1-x0)
	bpl		@startCalculations
	nop
	nop

	; swap the coordinates otherwise
	move	r0,r3	; safe for pipelining, we don't use r0 yet
	move	r3,r5
	move	r5,r0
	move	r0,r4
	move	r4,r6
	move	r6,r0

@startCalculations:
	ibt		r9, #1		; pipelined, yi = 1

	; r8 <- dx = x1 - x0
	to		r8
	from	r5
	sub		r3

	; r7 <- dy = y1 - y0
	to 		r7
	from 	r6
	sub		r4

	; r9 <- yi
	; if dY < 0: yi = -1, dy = -dy. else yi = 1
	bpl		@D_and_y
	nop
	nop

@negate_dY:
	ibt		r9, #$FF	; yi <- -1
	
	not		r7
	to		r7
	from	r7
	add		#1			; invert dY

@D_and_y:
	from	r0
	to		r0
	add		#0			; reset carry flag
	move	r12,r7
	rol		r12			; multiply by 2. r12 <- 2*dY
	to		r0
	from	r12
	sub		r8			; r0 = r6 - r8. (D = 2*dY - dX)

	move	r1, r3		; x = x0
	move	r2, r4		; y = y0

	; r11 <- 2*dX
	move	r11,r8
	add		#0			; reset carry flag
	rol		r11			; dX = dX * 2

@fromX0toX1:
	plot	; x++

	; if D > 0: y = y + yi; D = D - 2*dx
	sub		r0, #0		; sets negative flag
	bmi		@next
	nop

	; increase Y by yi and reset D
	to		r2
	from	r2
	add		r9

	; D = D - 2 * dx
	to		r0
	from	r0
	sub		r11

@next:
	; D = D + 2*dy
	to		r0
	from	r12
	add		r0			; r0 <- r0+r12

	from	r5
	cmp		r1			; result: x1-x0

	bne		@fromX0toX1

	gsu_POP	r11
	gsu_RTS

;;;;;;;;;;;;;;;;;
PlotLineHigh:
	gsu_PUSH	r11

@checkSwapY:
	; ensure y1 > y0
	to		r7
	from	r6
	sub		r4	; r7 = (y1-y0)
	bpl		@startCalculations
	nop
	nop

	; swap the coordinates otherwise
	move	r0,r3
	move	r3,r5
	move	r5,r0
	move	r0,r4
	move	r4,r6
	move	r6,r0

@startCalculations:
	ibt		r9, #1		; xi = 1

	; r7 <- dy = y1 - y0
	to 		r7
	from 	r6
	sub		r4

	; r8 <- dx = x1 - x0
	to		r8
	from	r5
	sub		r3

	; r9 <- yi
	; if dX < 0: xi = -1, dx = -dx. else xi = 1
	bpl		@D_and_x
	nop
	nop

@negate_dX:
	ibt		r9, #$FF	; xi <- -1
	
	not		r8
	to		r8
	from	r8
	add		#1			; invert dX

@D_and_x:
	from	r0
	to		r0
	add		#0			; reset carry flag
	move	r12,r8
	rol		r12			; multiply by 2. r12 <- 2*dX
	to		r0
	from	r12
	sub		r7			; r0 = r12 - r7. (D = 2*dX - dY)

	move	r1, r3		; x = x0
	move	r2, r4		; y = y0

	; r11 <- 2*dY
	move	r11,r7
	add		#0			; reset carry flag
	rol		r11			; dY = dY * 2

@fromY0toY1:
	plot		; x++...
	dec		r1	; ...x--
	inc		r2	; y++

	; if D > 0: x = x + xi; D = D - 2*dy
	sub		r0, #0		; sets negative flag
	bmi		@next
	nop

	; increase X by xi and reset D
	to		r1
	from	r1
	add		r9

	; D = D - 2 * dy
	to		r0
	from	r0
	sub		r11

@next:
	; D = D + 2*dx
	to		r0
	from	r12
	add		r0			; r0 <- r0+r12

	from	r5
	cmp		r1			; result: x1-x0

	bne		@fromY0toY1

	gsu_POP	r11
	gsu_RTS

;;;;;;;;

ClearFramebuffer:
	iwt		r0,#15
	from 	r0
	color

	iwt		r2,#-1
	iwt		r13,#@innerLoop

	iwt		r4, #192
	iwt		r5, #0

@outerLoop:
	iwt		r12,#192
	iwt		r1,#0
	inc		r2
	dec		r4

@innerLoop:
	plot
	loop

	; fell out of the loop - check our row index
	from	r4
	cmp		r5
	bne		@outerLoop	; loop if r4 != r5
	nop

	gsu_RTS

;;;;;;;;

SuperFX_DoSomething_End:
SuperFX_DoSomething_Length: .word (SuperFX_DoSomething_End - SuperFX_DoSomething)

BresenhamX0: 	.word 0
BresenhamY0: 	.word 0
BresenhamX1: 	.word 0
BresenhamY1: 	.word 0

degrees	= $F040

PointToRotateX0 = $F000
PointToRotateY0 = $F002
PointToRotateX1 = $F004
PointToRotateY1 = $F006

TempCosX = $F010
TempSinX = $F012
TempCosY = $F014
TempSinY = $F016

RotatedX0 = $F020
RotatedY0 = $F022
RotatedX1 = $F024
RotatedY1 = $F026

RotatePointInputX = $F030
RotatePointInputY = $F032

SinTable:	; 0 to 90 degrees. The rest can be extrapolated from this.
	.word $0000
	.word $0004
	.word $0008
	.word $000D
	.word $0011
	.word $0016
	.word $001A
	.word $001F
	.word $0023
	.word $0028
	.word $002C
	.word $0030
	.word $0035
	.word $0039
	.word $003D
	.word $0042
	.word $0046
	.word $004A
	.word $004F
	.word $0053
	.word $0057
	.word $005B
	.word $005F
	.word $0064
	.word $0068
	.word $006C
	.word $0070
	.word $0074
	.word $0078
	.word $007C
	.word $007F
	.word $0083
	.word $0087
	.word $008B
	.word $008F
	.word $0092
	.word $0096
	.word $009A
	.word $009D
	.word $00A1
	.word $00A4
	.word $00A7
	.word $00AB
	.word $00AE
	.word $00B1
	.word $00B5
	.word $00B8
	.word $00BB
	.word $00BE
	.word $00C1
	.word $00C4
	.word $00C6
	.word $00C9
	.word $00CC
	.word $00CF
	.word $00D1
	.word $00D4
	.word $00D6
	.word $00D9
	.word $00DB
	.word $00DD
	.word $00DF
	.word $00E2
	.word $00E4
	.word $00E6
	.word $00E8
	.word $00E9
	.word $00EB
	.word $00ED
	.word $00EE
	.word $00F0
	.word $00F2
	.word $00F3
	.word $00F4
	.word $00F6
	.word $00F7
	.word $00F8
	.word $00F9
	.word $00FA
	.word $00FB
	.word $00FC
	.word $00FC
	.word $00FD
	.word $00FE
	.word $00FE
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FE
	.word $00FE
	.word $00FD
	.word $00FC
	.word $00FC
	.word $00FB
	.word $00FA
	.word $00F9
	.word $00F8
	.word $00F7
	.word $00F6
	.word $00F4
	.word $00F3
	.word $00F2
	.word $00F0
	.word $00EE
	.word $00ED
	.word $00EB
	.word $00E9
	.word $00E8
	.word $00E6
	.word $00E4
	.word $00E2
	.word $00DF
	.word $00DD
	.word $00DB
	.word $00D9
	.word $00D6
	.word $00D4
	.word $00D1
	.word $00CF
	.word $00CC
	.word $00C9
	.word $00C6
	.word $00C4
	.word $00C1
	.word $00BE
	.word $00BB
	.word $00B8
	.word $00B5
	.word $00B1
	.word $00AE
	.word $00AB
	.word $00A7
	.word $00A4
	.word $00A1
	.word $009D
	.word $009A
	.word $0096
	.word $0092
	.word $008F
	.word $008B
	.word $0087
	.word $0083
	.word $007F
	.word $007C
	.word $0078
	.word $0074
	.word $0070
	.word $006C
	.word $0068
	.word $0064
	.word $005F
	.word $005B
	.word $0057
	.word $0053
	.word $004F
	.word $004A
	.word $0046
	.word $0042
	.word $003D
	.word $0039
	.word $0035
	.word $0030
	.word $002C
	.word $0028
	.word $0023
	.word $001F
	.word $001A
	.word $0016
	.word $0011
	.word $000D
	.word $0008
	.word $0004
	.word $0000
	.word $FFF9
	.word $FFF5
	.word $FFF0
	.word $FFEC
	.word $FFE7
	.word $FFE3
	.word $FFDE
	.word $FFDA
	.word $FFD5
	.word $FFD1
	.word $FFCD
	.word $FFC8
	.word $FFC4
	.word $FFC0
	.word $FFBB
	.word $FFB7
	.word $FFB3
	.word $FFAE
	.word $FFAA
	.word $FFA6
	.word $FFA2
	.word $FF9E
	.word $FF99
	.word $FF95
	.word $FF91
	.word $FF8D
	.word $FF89
	.word $FF85
	.word $FF81
	.word $FF7E
	.word $FF7A
	.word $FF76
	.word $FF72
	.word $FF6E
	.word $FF6B
	.word $FF67
	.word $FF63
	.word $FF60
	.word $FF5C
	.word $FF59
	.word $FF56
	.word $FF52
	.word $FF4F
	.word $FF4C
	.word $FF48
	.word $FF45
	.word $FF42
	.word $FF3F
	.word $FF3C
	.word $FF39
	.word $FF37
	.word $FF34
	.word $FF31
	.word $FF2E
	.word $FF2C
	.word $FF29
	.word $FF27
	.word $FF24
	.word $FF22
	.word $FF20
	.word $FF1E
	.word $FF1B
	.word $FF19
	.word $FF17
	.word $FF15
	.word $FF14
	.word $FF12
	.word $FF10
	.word $FF0F
	.word $FF0D
	.word $FF0B
	.word $FF0A
	.word $FF09
	.word $FF07
	.word $FF06
	.word $FF05
	.word $FF04
	.word $FF03
	.word $FF02
	.word $FF01
	.word $FF01
	.word $FF00
	.word $FEFF
	.word $FEFF
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFF
	.word $FEFF
	.word $FF00
	.word $FF01
	.word $FF01
	.word $FF02
	.word $FF03
	.word $FF04
	.word $FF05
	.word $FF06
	.word $FF07
	.word $FF09
	.word $FF0A
	.word $FF0B
	.word $FF0D
	.word $FF0F
	.word $FF10
	.word $FF12
	.word $FF14
	.word $FF15
	.word $FF17
	.word $FF19
	.word $FF1B
	.word $FF1E
	.word $FF20
	.word $FF22
	.word $FF24
	.word $FF27
	.word $FF29
	.word $FF2C
	.word $FF2E
	.word $FF31
	.word $FF34
	.word $FF37
	.word $FF39
	.word $FF3C
	.word $FF3F
	.word $FF42
	.word $FF45
	.word $FF48
	.word $FF4C
	.word $FF4F
	.word $FF52
	.word $FF56
	.word $FF59
	.word $FF5C
	.word $FF60
	.word $FF63
	.word $FF67
	.word $FF6B
	.word $FF6E
	.word $FF72
	.word $FF76
	.word $FF7A
	.word $FF7E
	.word $FF81
	.word $FF85
	.word $FF89
	.word $FF8D
	.word $FF91
	.word $FF95
	.word $FF99
	.word $FF9E
	.word $FFA2
	.word $FFA6
	.word $FFAA
	.word $FFAE
	.word $FFB3
	.word $FFB7
	.word $FFBB
	.word $FFC0
	.word $FFC4
	.word $FFC8
	.word $FFCD
	.word $FFD1
	.word $FFD5
	.word $FFDA
	.word $FFDE
	.word $FFE3
	.word $FFE7
	.word $FFEC
	.word $FFF0
	.word $FFF5
	.word $FFF9

CosTable:	; 0 to 90 degrees. The rest can be extrapolated from this.
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FE
	.word $00FE
	.word $00FD
	.word $00FC
	.word $00FC
	.word $00FB
	.word $00FA
	.word $00F9
	.word $00F8
	.word $00F7
	.word $00F6
	.word $00F4
	.word $00F3
	.word $00F2
	.word $00F0
	.word $00EE
	.word $00ED
	.word $00EB
	.word $00E9
	.word $00E8
	.word $00E6
	.word $00E4
	.word $00E2
	.word $00DF
	.word $00DD
	.word $00DB
	.word $00D9
	.word $00D6
	.word $00D4
	.word $00D1
	.word $00CF
	.word $00CC
	.word $00C9
	.word $00C6
	.word $00C4
	.word $00C1
	.word $00BE
	.word $00BB
	.word $00B8
	.word $00B5
	.word $00B1
	.word $00AE
	.word $00AB
	.word $00A7
	.word $00A4
	.word $00A1
	.word $009D
	.word $009A
	.word $0096
	.word $0092
	.word $008F
	.word $008B
	.word $0087
	.word $0083
	.word $0080
	.word $007C
	.word $0078
	.word $0074
	.word $0070
	.word $006C
	.word $0068
	.word $0064
	.word $005F
	.word $005B
	.word $0057
	.word $0053
	.word $004F
	.word $004A
	.word $0046
	.word $0042
	.word $003D
	.word $0039
	.word $0035
	.word $0030
	.word $002C
	.word $0028
	.word $0023
	.word $001F
	.word $001A
	.word $0016
	.word $0011
	.word $000D
	.word $0008
	.word $0004
	.word $0000
	.word $FFF9
	.word $FFF5
	.word $FFF0
	.word $FFEC
	.word $FFE7
	.word $FFE3
	.word $FFDE
	.word $FFDA
	.word $FFD5
	.word $FFD1
	.word $FFCD
	.word $FFC8
	.word $FFC4
	.word $FFC0
	.word $FFBB
	.word $FFB7
	.word $FFB3
	.word $FFAE
	.word $FFAA
	.word $FFA6
	.word $FFA2
	.word $FF9E
	.word $FF99
	.word $FF95
	.word $FF91
	.word $FF8D
	.word $FF89
	.word $FF85
	.word $FF81
	.word $FF7E
	.word $FF7A
	.word $FF76
	.word $FF72
	.word $FF6E
	.word $FF6B
	.word $FF67
	.word $FF63
	.word $FF60
	.word $FF5C
	.word $FF59
	.word $FF56
	.word $FF52
	.word $FF4F
	.word $FF4C
	.word $FF48
	.word $FF45
	.word $FF42
	.word $FF3F
	.word $FF3C
	.word $FF39
	.word $FF37
	.word $FF34
	.word $FF31
	.word $FF2E
	.word $FF2C
	.word $FF29
	.word $FF27
	.word $FF24
	.word $FF22
	.word $FF20
	.word $FF1E
	.word $FF1B
	.word $FF19
	.word $FF17
	.word $FF15
	.word $FF14
	.word $FF12
	.word $FF10
	.word $FF0F
	.word $FF0D
	.word $FF0B
	.word $FF0A
	.word $FF09
	.word $FF07
	.word $FF06
	.word $FF05
	.word $FF04
	.word $FF03
	.word $FF02
	.word $FF01
	.word $FF01
	.word $FF00
	.word $FEFF
	.word $FEFF
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFE
	.word $FEFF
	.word $FEFF
	.word $FF00
	.word $FF01
	.word $FF01
	.word $FF02
	.word $FF03
	.word $FF04
	.word $FF05
	.word $FF06
	.word $FF07
	.word $FF09
	.word $FF0A
	.word $FF0B
	.word $FF0D
	.word $FF0F
	.word $FF10
	.word $FF12
	.word $FF14
	.word $FF15
	.word $FF17
	.word $FF19
	.word $FF1B
	.word $FF1E
	.word $FF20
	.word $FF22
	.word $FF24
	.word $FF27
	.word $FF29
	.word $FF2C
	.word $FF2E
	.word $FF31
	.word $FF34
	.word $FF37
	.word $FF39
	.word $FF3C
	.word $FF3F
	.word $FF42
	.word $FF45
	.word $FF48
	.word $FF4C
	.word $FF4F
	.word $FF52
	.word $FF56
	.word $FF59
	.word $FF5C
	.word $FF60
	.word $FF63
	.word $FF67
	.word $FF6B
	.word $FF6E
	.word $FF72
	.word $FF76
	.word $FF7A
	.word $FF7E
	.word $FF81
	.word $FF85
	.word $FF89
	.word $FF8D
	.word $FF91
	.word $FF95
	.word $FF99
	.word $FF9E
	.word $FFA2
	.word $FFA6
	.word $FFAA
	.word $FFAE
	.word $FFB3
	.word $FFB7
	.word $FFBB
	.word $FFC0
	.word $FFC4
	.word $FFC8
	.word $FFCD
	.word $FFD1
	.word $FFD5
	.word $FFDA
	.word $FFDE
	.word $FFE3
	.word $FFE7
	.word $FFEC
	.word $FFF0
	.word $FFF5
	.word $FFF9
	.word $FFFE
	.word $0004
	.word $0008
	.word $000D
	.word $0011
	.word $0016
	.word $001A
	.word $001F
	.word $0023
	.word $0028
	.word $002C
	.word $0030
	.word $0035
	.word $0039
	.word $003D
	.word $0042
	.word $0046
	.word $004A
	.word $004F
	.word $0053
	.word $0057
	.word $005B
	.word $005F
	.word $0064
	.word $0068
	.word $006C
	.word $0070
	.word $0074
	.word $0078
	.word $007C
	.word $0080
	.word $0083
	.word $0087
	.word $008B
	.word $008F
	.word $0092
	.word $0096
	.word $009A
	.word $009D
	.word $00A1
	.word $00A4
	.word $00A7
	.word $00AB
	.word $00AE
	.word $00B1
	.word $00B5
	.word $00B8
	.word $00BB
	.word $00BE
	.word $00C1
	.word $00C4
	.word $00C6
	.word $00C9
	.word $00CC
	.word $00CF
	.word $00D1
	.word $00D4
	.word $00D6
	.word $00D9
	.word $00DB
	.word $00DD
	.word $00DF
	.word $00E2
	.word $00E4
	.word $00E6
	.word $00E8
	.word $00E9
	.word $00EB
	.word $00ED
	.word $00EE
	.word $00F0
	.word $00F2
	.word $00F3
	.word $00F4
	.word $00F6
	.word $00F7
	.word $00F8
	.word $00F9
	.word $00FA
	.word $00FB
	.word $00FC
	.word $00FC
	.word $00FD
	.word $00FE
	.word $00FE
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF
	.word $00FF

