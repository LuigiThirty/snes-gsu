	.include "lorom.inc"
	.include "equates.inc"
	.include "65816.inc"

	.export LoadVRAM

	.import GSU_LoadProgramIntoSRAM

; OAM sprite assignments
PlayerSpriteUL	= 4
PlayerSpriteUR	= 5
PlayerSpriteLL	= 6
PlayerSpriteLR	= 7

	.segment "CODE"

	.p816
	.a8
	.i16

	.zeropage

; Scratch pointer.
PtrTMP:
PtrTMP_Lo: .byte 0
PtrTMP_Hi: .byte 0

; Pointer to the RAM copy of the OAM.
PtrOAM:
PtrOAM_Lo: .byte 0
PtrOAM_Hi: .byte 0

Scratch:	.byte 0

; VBL flag.
VBlank_Flag: .byte 0

; Game state for our state machine.
GameState: .byte 0

	.code

reset:
	jmp Reset

;;;;;;;;;;;;;;;;;;;;;;

	.include "vram_macros.s"

;;;;;;;;;;
LoadVRAM:
	; - A:X is the source address for transfer
	; - Y is the number of bytes to copy

	; Save registers
	phb
	php

	stx $4302
	sta $4304
	sty $4305

	lda #$01
	sta $4300 	; DMA mode: word, normal increment
	lda #$18
	sta $4301	; write to VRAM write register
	lda #$01
	sta $420B	; begin DMA transfer on Channel 0

	; Restore registers and return
	plp
	plb

	rts

;;;;;;;;;;;;;;;;;;;;;;;;

Reset:
	clc		; native mode
	xce
	rep #$10	; X/Y 16-bit
	sep #$20	; A 8-bit

Start:
	; force VBL
	lda	#$80
	sta INIDISP

	; set up SuperFX
	jsr	ZeroSRAM
	jsr GSU_LoadProgramIntoSRAM

	;;; Set up OAM
	ldx	#OAM_RAMCopy
	stx	PtrOAM
	jsr	InitializeOAM

	; Clear tilemaps
	jsr	ClearTilemapSC0
	jsr	SetupBG3

	lda #$80
	sta VMAINC 		; Word-access, increment by 1

	; Load 2BPP graphics tiles into VRAM at $C000.
	ldx	#$6000	; Destination address in words
	stx	VMADDR	; sav
	lda #$01	; source bank
	ldx	#$C000	; source offset
	ldy	#$4000	; size to transfer
	jsr	LoadVRAM

	; Load 4BPP graphics tiles into VRAM at $4000.
	ldx	#$2000	; Destination address in words
	stx	VMADDR	; save
	lda #$01	; source bank
	ldx	#$8000	; source offset
	ldy	#$4000	; size to transfer
	jsr	LoadVRAM

	lda #$08
	sta BG1SC		; BG1 starts at $1000
	lda	#$0C
	sta	BG2SC		; BG2 starts at $1800

	lda	#$42
	sta BG12NBA		; BG1 tiles are at $4000
					; BG2 tiles are at $8000

	lda	#$0C		 
	sta	BG3SC		; BG3 starts at $1800

	lda	#$06
	sta BG34NBA		; BG3 tiles are at $C000

	; change the background color
	stz CGADD		; select address $0 in CG-RAM

	stz	WRCGDATA
	stz	WRCGDATA	; write black to index

	lda	#0
	sta WRCGDATA
	lda	#%11100000
	sta WRCGDATA	; write blue to index 1

	lda	#%10000000
	sta WRCGDATA
	lda	#%00000011
	sta WRCGDATA	; green

	; %0111001110011100
	lda	#%11110111
	sta	WRCGDATA
	lda	#%11011110
	sta	WRCGDATA	; gray

	; Sprite palettes start at entry 128.
	lda #128
	sta	CGADD

	stz WRCGDATA
	stz	WRCGDATA	; black

	lda	#%00011100
	sta	WRCGDATA
	lda	#0
	sta	WRCGDATA	; red

	lda	#%10000000
	sta WRCGDATA
	lda	#%00000011
	sta WRCGDATA	; green

	lda	#0
	sta WRCGDATA
	lda	#%01110000
	sta WRCGDATA	; blue

	jsr	LoadStatusAreaPalette

	M_PrintString STR_Status, ($18F2 >> 1)
	M_PrintString STR_Area, ($1932 >> 1)
	M_PrintString STR_SuperFXArea, ($1B02 >> 1)
	M_PrintString STR_Remainder, ($1E02 >> 1)

	M_PrintString STR_What, ($1C00 >> 1)

	; Init player sprite
	lda	#100
	sta	PlayerPosX
	sta	PlayerPosY

SetSprite:
	ldy	#0

	lda	#120
	sta	(PtrOAM),Y
	iny

	lda	#120
	sta (PtrOAM),Y
	iny
	
	lda	#1
	sta	(PtrOAM),Y
	iny

	lda	#0
	sta (PtrOAM),Y

	jsr	UpdateOAM

;	jsr	GSU_CopyGSUBufferToBG3
	jsr	DrawTitle

GoFrame:
	lda #1
	sta BGMODE 		; Mode 1 graphics

	lda	#%00000001
	sta	OBJSEL		; object size and start in VRAM.

	lda	#%00010111
	sta	TM			; enable BG1/BG2/BG3 and sprites

	lda	#%00001111	; end VBL, screen max brightness
	sta INIDISP

	lda	#$81
	sta NMITIMEN	; enable VBLANK NMI and joypad i/o

	lda	#$C0
	sta	WRIO		; automatic joypad reads

	jmp	VS_FrameLoop	; and proceed to our frame loop

;;;

DrawTitle:
	phb
	php

	ldx	#$0908
	stx	VMADDR
	ldy	#16

	; use data bank 1
	lda	#$01
	pha
	plb

	lda	#$A0

@row1loop:	
	stx VMADDR
	sta	VMDATAL

	ina
	inx
	dey
	cpy #0
	bne	@row1loop

	ldx	#$0928
	stx	VMADDR
	ldy	#16
	lda	#$B0

@row2loop:	
	stx VMADDR
	sta	VMDATAL

	ina
	inx
	dey
	cpy #0
	bne	@row2loop

	ldx	#$0948
	stx	VMADDR
	ldy	#16
	lda	#$C0

@row3loop:	
	stx VMADDR
	sta	VMDATAL

	ina
	inx
	dey
	cpy #0
	bne	@row3loop

	ldx	#$0968
	stx	VMADDR
	ldy	#16
	lda	#$D0

@row4loop:	
	stx VMADDR
	sta	VMDATAL

	ina
	inx
	dey
	cpy #0
	bne	@row4loop

	plp
	plb

	rts

;;;
	.include "vshooter.s"
;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; String functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
PrintString:
	; 16-bit index required
	; IN: 	- 16-bit string address in Y
	;		- 16-bit VRAM destination in X
	; OUT:	- n/a
	phb
	php

	sty	PtrTMP
	ldy	#0

	lda	(PtrTMP),Y
@loop:
	stx	VMADDR		; set destination VRAM address
	sta	VMDATAL		; write character to VRAM
	stz	VMDATAH

	inx 			; increment tile index
	iny 			; increment string index
	lda	(PtrTMP),Y	; get string character

	cmp	#0			; null?
	bne	@loop		; loop until null

	plp
	plb

	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; OAM functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
InitializeOAM:
	; Initialize our RAM OAM copy.
	phb
	php

	; Reset the whole OAM to 0.
	ldy	#$200
	lda	#0
@loop:
	sta	(PtrOAM),Y
	dey
	cpy	#0
	bne	@loop

	plp
	plb
	rts

;;;

UpdateOAM:
	stz	$2102
	stz	$2103			; OAM write address reset to $0

	lda #$00
	sta DMACH0SETTINGS 	; DMA mode: byte, normal increment
	lda #$04
	sta DMACH0DEST		; write to OAM

	stz	DMACH0BANK		; read from bank 0
	ldx	#OAM_RAMCopy
	stx	DMACH0OFFSET	; read from our OAM copy
	ldx #512
	stx	DMACH0BYTES

	lda #$01
	sta MDMAEN			; begin DMA transfer on Channel 0

	rts

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BG functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ClearTilemapSC0:
	; 16-bit indexes, 8-bit accumulator

	phb
	php

	ldx	#$0800
	ldy	#$400
@loop:
	stx VMADDR
	stz	VMDATAL
	inx
	dey

	cpy	#0
	bne	@loop	; loop $400 times, setting the tilemap of Screen 0 to tile 0

	plp
	plb
	rts

SetupBG2:
	GoACC16

	; Set up BG2 as a GSU-powered background with a software tilemap.
	lda	#0
	ldx	#$0C00
	ldy	#32

	stz Scratch

@outerloop:

@innerloop:
	stx VMADDR
	sta	VMDATA

	clc
	adc	#$18
	inx
	dey
	cpy	#0
	bne	@innerloop

	ldy	#32

	inc	Scratch
	lda Scratch
	cmp	#$18
	bne	@outerloop

	GoACC8

	rts

;;;;;;;;

SetupBG3:
	jmp	@StatusArea

	GoACC16

	; Set up BG3 as a 24x24 GSU-powered background with a software tilemap.
	lda	#0
	ldx	#$0C00
	ldy	#32

	stz Scratch

@outerloop:

@innerloop:
	stx VMADDR
	sta	VMDATA

	clc
	adc	#$18
	inx
	dey
	cpy	#0
	bne	@innerloop

	ldy	#32

	inc	Scratch
	lda Scratch
	cmp	#$18
	bne	@outerloop

	GoACC8

@StatusArea:

; Second time - right side
	lda	#32
	ldx	#$0C18

@rowLoop:
	pha

	GoACC16
	txa
	clc
	adc	#32
	tax
	GoACC8

	pla

	stx VMADDR
	ldy	#$13C1	; palette 1, tile $3C1
	sty	VMDATA
	ldy	#$13C2	; palette 2, tile $3C2
	sty	VMDATA
	sty	VMDATA
	sty	VMDATA
	sty	VMDATA
	sty	VMDATA
	sty	VMDATA
	sty	VMDATA

	dec
	cmp	#0
	bne	@rowLoop

	rts

;;;;;
ZeroSRAM:
	ldx	#$FFFF
	lda	#0

@loop:
	sta	$700000,X
	dex
	cpx	#0
	bne	@loop

	rts

;;;;;

LoadStatusAreaPalette:
	lda #16
	sta	CGADD

	stz WRCGDATA
	stz	WRCGDATA	; black

	lda	#%00011100
	sta	WRCGDATA
	lda	#0
	sta	WRCGDATA	; red

	lda	#%01110011
	sta	WRCGDATA
	lda	#%11001110
	sta	WRCGDATA	; dk gray

	; %0111001110011100
	lda	#%11110111
	sta	WRCGDATA
	lda	#%11011110
	sta	WRCGDATA	; lt gray

	rts

;;;;;
.include "strings.s"

;;;;;
VBLANK:
	; VBLANK interrupt.
	rti

;;;;;
	.bss
OAM_RAMCopy: .res $200, 0

PlayerPosX: .byte 0
PlayerPosY: .byte 0

; Joypad 1 word
Joypad1:
Joypad1L:	.byte 0
Joypad1H:	.byte 0

SuperFX_WRAM: .res $200, 0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	.segment "BANK1"

	; BANK1 is accessible in page $01
	; $018000-$01FFFF ==> BANK1 $0000-$7FFF

GFXData: 	; $0000
	.incbin "gfx/gfx.bin"

ASCII2BPP: 	; $4000
	.incbin	"gfx/gfx2bpp.bin"
