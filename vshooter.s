	.import SuperFX_DoSomething
	.import SuperFX_DoSomething_End
	.import SuperFX_DoSomething_Length

	.import GSU_CopyGSUBufferToBG2_ONE
	.import GSU_CopyGSUBufferToBG2_TWO
	.import GSU_CopyGSUBufferToBG2
	.import GSU_CopyGSUBufferToBG3

	.import GSU_ClearGSUPixelBuffer
	.import GSU_WaitForProgram

	.bss
even_frame:		.byte 0
	.code

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Vertical Shooter level logic ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
VS_FrameLoop:
	wai

	lda	#$80
	sta	$2100
	
;	jsr	GSU_CopyGSUBufferToBG3

;	lda	#%00001101
;	sta	$2100
;	wai
;	lda	#$80
;	sta	$2100

	; Check settings and start SuperFX program.
	lda	#$01
	sta	SFX_CLSR	; 21.4MHz GSU.

	lda	#%10100000
	sta	SFX_CFGR	; GSU IRQ on STOP is masked.

	lda	#%00010000
	sta	SFX_SCBR

	lda	#$70
	sta	SFX_PBR		; use cart RAM bank

	lda	#%00101000
	sta	SFX_SCMR	; SuperFX gets cart RAM access

	ldx #$0
	stx	SFX_R15		; set the SuperFX PC

	; Read joypad
	lda	STDCNTRL1L
	sta	Joypad1L
	lda	STDCNTRL1H
	sta	Joypad1H

	JSR VS_HandlePlayerShipInput
	JSR	VS_PositionPlayerSprite

	lda	(PtrOAM)
	ina
	sta	(PtrOAM)

	jsr	UpdateOAM

	; Wait for the GSU to finish, if it's still running.
	jsr	GSU_WaitForProgram

	lda	#%00100001
	sta	SFX_SCMR	; SNES gets cart RAM access

	lda	#%00001101
	sta	$2100

	jmp	VS_FrameLoop

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
VS_HandlePlayerShipInput:

@checkR:
	lda	Joypad1H
	and	#$01
	cmp	#$01
	bne @checkL

	inc	PlayerPosX

@checkL:
	lda	Joypad1H
	and	#$02
	cmp	#$02
	bne @checkU

	dec	PlayerPosX

@checkU:
	lda	Joypad1H
	and	#$08
	cmp	#$08
	bne @checkD

	dec	PlayerPosY

@checkD:
	lda	Joypad1H
	and	#$04
	cmp	#$04
	bne @joypadDone

	inc	PlayerPosY

@joypadDone:
	rts


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
VS_PositionPlayerSprite:
	; The player is made up of 4 sprites.

	phb
	php
	
@UL:
	ldy	#(PlayerSpriteUL*4)	

	lda	PlayerPosX
	sta	(PtrOAM),Y
	iny

	lda	PlayerPosY
	sta (PtrOAM),Y
	iny
	
	lda	#$80
	sta	(PtrOAM),Y
	iny

	lda	#0
	sta (PtrOAM),Y

@UR:
	ldy	#(PlayerSpriteUR*4)	

	lda	PlayerPosX
	clc
	adc	#8
	sta	(PtrOAM),Y
	iny

	lda	PlayerPosY
	sta (PtrOAM),Y
	iny
	
	lda	#$81
	sta	(PtrOAM),Y
	iny

	lda	#0
	sta (PtrOAM),Y

@LL:
	ldy	#(PlayerSpriteLL*4)	

	lda	PlayerPosX
	sta	(PtrOAM),Y
	iny

	lda	PlayerPosY
	clc
	adc	#8
	sta (PtrOAM),Y
	iny
	
	lda	#$90
	sta	(PtrOAM),Y
	iny

	lda	#0
	sta (PtrOAM),Y

@LR:
	ldy	#(PlayerSpriteLR*4)	

	lda	PlayerPosX
	clc
	adc	#8
	sta	(PtrOAM),Y
	iny

	lda	PlayerPosY
	clc
	adc	#8
	sta (PtrOAM),Y
	iny
	
	lda	#$91
	sta	(PtrOAM),Y
	iny

	lda	#0
	sta (PtrOAM),Y

	plp
	plb
	rts