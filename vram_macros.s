.macro LoadBlockToVRAM SRCADDR, DEST, SIZE 
	lda #$80
	sta VMAINC 		; Word-access, increment by 1

	ldx	#DEST		; Destination address
	stx	VMADDR		; save
	lda #^SRCADDR	; source bank
	ldx	#SRCADDR	; source offset
	ldy	#SIZE		; size to transfer
	jsr	LoadVRAM
.endmacro

.macro M_PrintString STRING, DEST
	ldx	#DEST
	ldy	#STRING
	jsr	PrintString
.endmacro