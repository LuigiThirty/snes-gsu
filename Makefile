AS=ca65
LD=ld65

ASFLAGS = -g --cpu 65816
LDFLAGS = -C lorom128.cfg

OBJDIR = obj

all: hello.smc

$(OBJDIR)/hello.o: hello.s vshooter.s strings.s gsu.s
	$(AS) $(ASFLAGS) -l hello.lst -o $@ $<

$(OBJDIR)/gsu.o: gsu.s
	$(AS) $(ASFLAGS) -l gsu.lst -o $@ $<

$(OBJDIR)/sfxdemo.o: sfxdemo.s
	$(AS) $(ASFLAGS) -l sfxdemo.lst -o $@ $<

hello.smc: $(OBJDIR)/hello.o $(OBJDIR)/sfxdemo.o $(OBJDIR)/gsu.o
	$(LD) $(LDFLAGS) -m hello.map -o $@ $^

.phony: all clean

clean:
	rm $(OBJDIR)/*.o
	rm hello.smc
