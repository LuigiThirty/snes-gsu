------+-------------------+-------------+----+---------+------+-----------------------+-------------------------------------------------------------------
 Line | # File       Line | Line Type   | MX |  Reloc  | Size | Address   Object Code |  Source Code                                                      
------+-------------------+-------------+----+---------+------+-----------------------+-------------------------------------------------------------------
    1 |  1 hello.s      1 | Unknown    | ?? |         |   -1 | 00/FFFF               | .include      "lorom.inc"                     
    2 |  1 hello.s      2 | Unknown    | ?? |         |   -1 | 00/FFFF               | .include      "equates.inc"                   
    3 |  1 hello.s      3 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
    4 |  1 hello.s      4 | Unknown    | ?? |         |   -1 | 00/FFFF               |               .segment         "CODE"         
    5 |  1 hello.s      5 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
    6 |  1 hello.s      6 | Unknown    | ?? |         |   -1 | 00/FFFF               |               .a8                             
    7 |  1 hello.s      7 | Unknown    | ?? |         |   -1 | 00/FFFF               |               .i16                            
    8 |  1 hello.s      8 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
    9 |  1 hello.s      9 | Unknown    | ?? |         |   -1 | 00/FFFF               |               .export          GFXData        
   10 |  1 hello.s     10 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   11 |  1 hello.s     11 | Empty       | ?? |         |   -1 | 00/FFFF               | reset:                                        
   12 |  1 hello.s     12 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               jmp              Reset          
   13 |  1 hello.s     13 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   14 |  1 hello.s     14 | Comment     | ?? |         |   -1 | 00/FFFF               | ;;;;;;;;;;;;;;;;;;;;;;
   15 |  1 hello.s     15 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   16 |  1 hello.s     16 | Unknown    | ?? |         |   -1 | 00/FFFF               |               .include         "vram_macros.s"
   17 |  1 hello.s     17 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   18 |  1 hello.s     18 | Comment     | ?? |         |   -1 | 00/FFFF               | ;;;;;;;;;;
   19 |  1 hello.s     19 | Empty       | ?? |         |   -1 | 00/FFFF               | LoadVRAM:                                     
   20 |  1 hello.s     20 | Comment     | ?? |         |   -1 | 00/FFFF               | 	; - A:X is the source address for transfer
   21 |  1 hello.s     21 | Comment     | ?? |         |   -1 | 00/FFFF               | 	; - Y is the number of bytes to copy
   22 |  1 hello.s     22 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   23 |  1 hello.s     23 | Comment     | ?? |         |   -1 | 00/FFFF               | 	; Save registers
   24 |  1 hello.s     24 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               phb                             
   25 |  1 hello.s     25 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               php                             
   26 |  1 hello.s     26 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   27 |  1 hello.s     27 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               stx              $4302          
   28 |  1 hello.s     28 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              $4304          
   29 |  1 hello.s     29 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sty              $4305          
   30 |  1 hello.s     30 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   31 |  1 hello.s     31 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #$01           
   32 |  1 hello.s     32 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              $4300          ; DMA mode: word, normal increment
   33 |  1 hello.s     33 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #$18           
   34 |  1 hello.s     34 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              $4301          ; write to VRAM write register
   35 |  1 hello.s     35 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #$01           
   36 |  1 hello.s     36 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              $420B          ; begin DMA transfer on Channel 1
   37 |  1 hello.s     37 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   38 |  1 hello.s     38 | Comment     | ?? |         |   -1 | 00/FFFF               | 	; Restore registers and return
   39 |  1 hello.s     39 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               plp                             
   40 |  1 hello.s     40 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               plb                             
   41 |  1 hello.s     41 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   42 |  1 hello.s     42 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               rts                             
   43 |  1 hello.s     43 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   44 |  1 hello.s     44 | Comment     | ?? |         |   -1 | 00/FFFF               | ;;;;;;;;;;;;;;;;;;;;;;;;
   45 |  1 hello.s     45 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   46 |  1 hello.s     46 | Empty       | ?? |         |   -1 | 00/FFFF               | Reset:                                        
   47 |  1 hello.s     47 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               clc                             ; native mode
   48 |  1 hello.s     48 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               xce                             
   49 |  1 hello.s     49 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               rep              #$10           ; X/Y 16-bit
   50 |  1 hello.s     50 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sep              #$20           ; A 8-bit
   51 |  1 hello.s     51 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   52 |  1 hello.s     52 | Empty       | ?? |         |   -1 | 00/FFFF               | Start:                                        
   53 |  1 hello.s     53 | Comment     | ?? |         |   -1 | 00/FFFF               | 	; force VBL
   54 |  1 hello.s     54 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #$80           
   55 |  1 hello.s     55 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              INIDISP        
   56 |  1 hello.s     56 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   57 |  1 hello.s     57 | Unknown    | ?? |         |   -1 | 00/FFFF               |               LoadBlockToVRAM  GFXData, $0, $20
   58 |  1 hello.s     58 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   59 |  1 hello.s     59 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #2             
   60 |  1 hello.s     60 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              BGMODE         ; Mode 0 graphics
   61 |  1 hello.s     61 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   62 |  1 hello.s     62 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #$04           
   63 |  1 hello.s     63 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              BG1SC          ; BG1 tilemap VRAM offset is $0400. 32x32 tilemap
   64 |  1 hello.s     64 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               stz              BG12NBA        ; BG1 character VRAM offset is $0000
   65 |  1 hello.s     65 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   66 |  1 hello.s     66 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #$01           
   67 |  1 hello.s     67 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              TM             ; enable BG1
   68 |  1 hello.s     68 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   69 |  1 hello.s     69 | Comment     | ?? |         |   -1 | 00/FFFF               | 	; change the background color
   70 |  1 hello.s     70 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #0             
   71 |  1 hello.s     71 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              CGADD          
   72 |  1 hello.s     72 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              CGADD          ; select address $0000 in CG-RAM
   73 |  1 hello.s     73 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   74 |  1 hello.s     74 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #0             
   75 |  1 hello.s     75 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              WRCGDATA       
   76 |  1 hello.s     76 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #%11100000     
   77 |  1 hello.s     77 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              WRCGDATA       ; write 16 bits to CG-RAM
   78 |  1 hello.s     78 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   79 |  1 hello.s     79 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               lda              #%00001111     ; end VBL, screen max brightness
   80 |  1 hello.s     80 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               sta              INIDISP        
   81 |  1 hello.s     81 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   82 |  1 hello.s     82 | Empty       | ?? |         |   -1 | 00/FFFF               | LoopForever:                                  
   83 |  1 hello.s     83 | Code        | ?? |         |   -1 | 00/FFFF : 00          |               jmp              LoopForever    
   84 |  1 hello.s     84 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   85 |  1 hello.s     85 | Comment     | ?? |         |   -1 | 00/FFFF               | ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   86 |  1 hello.s     86 | Empty       | ?? |         |   -1 | 00/FFFF               | GFXData:                                      
   87 |  1 hello.s     87 | Unknown    | ?? |         |   -1 | 00/FFFF               |               .incbin          "gfx.bin"      
   88 |  1 hello.s     88 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
   89 |  1 hello.s     89 | Empty       | ?? |         |   -1 | 00/FFFF               |                                               
------+-------------------+-------------+----+---------+------+-----------------------+-------------------------------------------------------------------
